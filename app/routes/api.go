package routes

import (
	"go-learning/app/controller"
	"net/http"

	"github.com/labstack/echo/v4"
	guard "github.com/labstack/echo/v4/middleware"
)

func RegisterApi(e *echo.Echo) {
	e.Use(guard.CORSWithConfig(guard.CORSConfig{
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	apiGroup := e.Group("/api")
	initApiV1(apiGroup.Group("/v1"))
}

func initApiV1(g *echo.Group) {
	g.GET("/users", controller.GetUser)
}
