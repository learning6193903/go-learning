package controller

import (
	"go-learning/app/model"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

func GetUser(c echo.Context) error {
	logrus.Infoln("======Start=========")

	user := []model.User{}
	if err := model.DB.Find(&user).Error; err != nil {
		logrus.Errorln("err =", err)
	}

	return c.JSON(http.StatusOK, user)
	// return c.JSON(http.StatusOK, struct{ Status string }{Status: "OK"})
}
