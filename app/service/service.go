package service

import (
	"fmt"
	"os"

	"go-learning/app/model"

	"github.com/sirupsen/logrus"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// /////// file นี้ไม้ได้ใช้งาน
var DB *gorm.DB

var err error

func InnitService() error {
	if err := InnitDatabase(); err != nil {
		return err
	}
	if err := InnitDatabaseStructure(); err != nil {
		return err
	}
	return nil
}

func InnitDatabase() error {
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?parseTime=true",
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_NAME"),
	)

	DB, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		logrus.Errorln("DB can't connect")
		return err
	} else {
		logrus.Infoln("Connect to ", dsn)
	}

	DB.AutoMigrate(&model.User{})
	return nil
}

func InnitDatabaseStructure() error {
	return nil
}
